# pylayout

Python module to get/set keyboard layout

## Fixes

https://superuser.com/questions/1360623/cant-remove-unneeded-keyboard-layouts-no-such-setting-anywhere
https://superuser.com/questions/957552/how-to-delete-a-keyboard-layout-in-windows-10/1340511#1340511
